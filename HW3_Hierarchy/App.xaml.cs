﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HW3_Hierarchy
{
    public partial class App : Application
    {
        public static double r;
        public static double g;
        public static double b;
        public static string lastFood="";
        public static int wins;
        public static int loses;
        public static int tie;
        public App()
        {
            InitializeComponent();

            MainPage = new NavigationPage(new MainPage());
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
