﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace HW3_Hierarchy
{
    public partial class GamePage : ContentPage
    {
        int usersChoice=-1;
        int appChoice;
        int winC;
        int loseC;
        int tieC;
        Random rand = new Random();
        public GamePage()
        {
            InitializeComponent();
        }
        void PlayButton(object sender, System.EventArgs e)
        {
            appChoice=rand.Next(3);
            ties.Text = ("Ties: "+ tieC);
            wins.Text = ("Wins: "+ winC);
            loses.Text = ("Loses: "+ loseC);

            if (usersChoice < 0 || usersChoice > 2)
            {
                DisplayAlert("Alert","Choose an option first","Ok");
            }
            else
            {
                if (appChoice == 0)
                {
                    compChoice.Source = "Rock";
                    if (usersChoice == 0)
                    {
                        tieC++;
                        ties.Text = ("Ties: " + tieC);
                    }
                    else if (usersChoice == 1)
                    {
                        winC++;
                        wins.Text = ("Wins: " + winC);
                    }
                    else if (usersChoice == 2)
                    {
                        loseC++;
                        loses.Text = ("Loses: " + loseC);
                    }

                }
                else if (appChoice == 1)
                {
                    compChoice.Source = "Paper";
                    if (usersChoice == 0)
                    {
                        loseC++;
                        loses.Text = ("Loses: " + loseC);
                    }
                    else if (usersChoice == 1)
                    {
                        tieC++;
                        ties.Text = ("Ties: " + tieC);
                    }
                    else if (usersChoice == 2)
                    {
                        winC++;
                        wins.Text = ("Wins: " + winC);
                    }

                }
                else if (appChoice == 2)
                {
                    compChoice.Source = "Scissor";
                    if (usersChoice == 0)
                    {
                    
                        winC++;
                        wins.Text = ("Wins: " + winC);
                    }
                    else if (usersChoice == 1)
                    {

                        loseC++;
                        loses.Text = ("Loses: " + loseC);
                    }
                    else if (usersChoice == 2)
                    {
                        tieC++;
                        ties.Text = ("Ties: " + tieC);
                    }

                }
            }

        }
        void RockButton(object sender, System.EventArgs e)
        {
            usersChoice = 0;
            userChoice.Source = "Rock";

        }
        void PaperButton(object sender, System.EventArgs e)
        {
            usersChoice = 1;
            userChoice.Source = "Paper";
        }
        void ScissorButton(object sender, System.EventArgs e)
        {
            usersChoice = 2;
            userChoice.Source = "Scissor";
        }
        void OnAppear(object sender, System.EventArgs e)
        {
            tieC = App.tie;
            winC = App.wins;
            loseC = App.loses;
            wins.Text = "Total wins: " + winC;
            loses.Text = "Total loses: " + loseC;
            ties.Text = "Total ties: " + tieC;
        }
        void OnDisappear(object sender, System.EventArgs e)
        {
            App.wins = winC;
            App.loses = loseC;
            App.tie = tieC;
        }
    }
}
