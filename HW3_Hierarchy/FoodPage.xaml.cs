﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace HW3_Hierarchy
{
    public partial class FoodPage : ContentPage
    {
        string lastFood;
        public FoodPage()
        {
            InitializeComponent();
        }
        void PizzaButton(object sender, System.EventArgs e)
        {
            Navigation.PushAsync(new PizzaPage());
            lastFood = "pizza";
        }
        void BurgerButton(object sender, System.EventArgs e)
        {
            Navigation.PushAsync(new BurgerPage());
            lastFood = "burger";
        }
        void IceCreamButton(object sender, System.EventArgs e)
        {
            Navigation.PushAsync(new IceCreamPage());
            lastFood = "ice Cream";
        }
        void OnAppear(object sender, System.EventArgs e)
        {

            if (App.lastFood != "")
            {
                lastFood = App.lastFood;
                askAgain.Text = "Would you like to eat " + lastFood + " again?";
            }
        }
        void OnDisappear(object sender, System.EventArgs e)
        {
            App.lastFood = lastFood;

        }
    }
}
