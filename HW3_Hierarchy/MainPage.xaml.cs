﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace HW3_Hierarchy
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        
        public MainPage()
        {
            InitializeComponent();
        }

        void GameButton(object sender, System.EventArgs e)
        {
            Navigation.PushAsync(new GamePage());
        }
        void ColorsButton(object sender, System.EventArgs e)
        {
            Navigation.PushAsync(new ColorsPage());
        }
        void FoodButton(object sender, System.EventArgs e)
        {
            Navigation.PushAsync(new FoodPage());
        }
    }
}
